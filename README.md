# Getting Started
1. `git clone https://gitlab.com/nikunj_seven_square/laravel-message-app.git`
1. `cd laravel-message-app`
1. `composer install`
1. `copy .env.example as .env`
1. `php artisan key:generate`
1. Update your `.env` file with your `LARASOCKET_TOKEN`. You can get one for free at [Larasocket](https://larasocket.com)
1. `php artisan migrate`
1. `php artisan serve`
1. Head over to: http://localhost:8000/chat to try out the demo.
